from kivymd.uix.card import MDCard
from kivy.properties import StringProperty
from kivy.uix.screenmanager import Screen


class MainScreenView(Screen):
    pass


class TestCard(MDCard):
    title = StringProperty()
    count = StringProperty()
    transition = StringProperty()
