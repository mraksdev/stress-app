from kivy.uix.screenmanager import Screen
from kivymd.uix.card import MDCard
from kivy.properties import StringProperty
from assets.data.results_templates import SCALE_RESULTS
import json


class ScaleScreenView(Screen):
    pass


class QuizCard(MDCard):
    question = StringProperty()
    group = StringProperty()
    answ = None

    def set_answer(self, value):
        SCALE_RESULTS[self.answ] = value
        json.dump(SCALE_RESULTS, open("assets/data/results.json", "w"))
        type = {"type": 1}
        json.dump(type, open("assets/data/type.json", "w"))
