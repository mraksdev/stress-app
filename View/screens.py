from View.MainScreen.main_screen import MainScreenView
from View.ScaleScreen.scale_screen import ScaleScreenView
from View.ExpScreen.exp_screen import ExpScreenView
from View.PSMScreen.psm_screen import PSMScreenView
from View.SanScreen.san_screen import SanScreenView
from View.ResultScreen.result_screen import ResultScreenView

screens = {
    "main screen": {
        "view": MainScreenView,
        "kv": "View/MainScreen/main_screen.kv",
    },
    "scale screen": {
        "view": ScaleScreenView,
        "kv":  "View/ScaleScreen/scale_screen.kv"
    },
    "exp screen": {
        "view": ExpScreenView,
        "kv": "View/ExpScreen/exp_screen.kv",
    },
    "psm screen": {
        "view": PSMScreenView,
        "kv": "View/PSMScreen/psm_screen.kv",
    },
    "san screen": {
        "view": SanScreenView,
        "kv": "View/SanScreen/san_screen.kv",
    },
    "result screen": {
        "view": ResultScreenView,
        "kv": "View/ResultScreen/result_screen.kv",
    },
}
