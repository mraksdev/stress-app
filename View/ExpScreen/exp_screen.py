from kivy.uix.screenmanager import Screen
from kivymd.uix.card import MDCard
from kivy.properties import StringProperty
from assets.data.results_templates import EXP_RESULTS
import json


class ExpScreenView(Screen):
    pass


class ExpCard(MDCard):
    question = StringProperty()
    group = StringProperty()
    answ = None

    def set_answer(self, value):
        EXP_RESULTS[self.answ] = value
        json.dump(EXP_RESULTS, open("assets/data/results.json", "w"))
        type = {"type": 4}
        json.dump(type, open("assets/data/type.json", "w"))
