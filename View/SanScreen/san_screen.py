from kivy.uix.screenmanager import Screen
from kivymd.uix.card import MDCard
from kivy.properties import StringProperty
from assets.data.results_templates import SAN_RESULTS
import json


class SanScreenView(Screen):
    pass


class SanCard(MDCard):
    question = StringProperty()
    group = StringProperty()
    answ = None

    def set_answer(self, value):
        SAN_RESULTS[self.answ] = value
        json.dump(SAN_RESULTS, open("assets/data/san_results.json", "w"))
        type = {"type": 2}
        json.dump(type, open("assets/data/type.json", "w"))
