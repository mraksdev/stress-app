from kivy.uix.screenmanager import Screen
from kivy.properties import StringProperty
from kivymd.uix.card import MDCard
import json


class ResultScreenView(Screen):
    result = StringProperty()
    title = StringProperty()

    def show_results(self):
        self.pick_brain_type()

    def pick_brain_type(self):
        test_type = json.load(open("assets/data/type.json"))

        if test_type["type"] == 1:
            self.title = "Шкала воспринимаемого стресса"
            self.result = self.calculate_scale(json.load(open("assets/data/results.json")))
        elif test_type["type"] == 2:
            self.title = "Опросник САН"
            self.result = self.calculate_san(json.load(open("assets/data/results.json")))
        elif test_type["type"] == 3:
            self.title = "Шкала PSM-25"
            self.result = self.calculate_psm(json.load(open("assets/data/results.json")))
        elif test_type["type"] == 4:
            self.title = "Експресс тест"
            self.result = self.calculate_express(json.load(open("assets/data/results.json")))

    def calculate_scale(self, answers):
        exc = ["4", "5", "7", "8"]
        total = 0
        result_str = ""

        for key in answers:
            if key in exc:
                answers[key] = abs(answers[key] - 4)
            total = answers[key] + total

        if total <= 13:
            result_str = f"У вас - Низкий уровень стресса \nколичество баллов - {total}"
        elif 13 < total <= 26:
            result_str = (
                f"У вас - Средний уровень стресса \nколичество баллов - {total}"
            )

        elif 26 < total <= 40:
            result_str = (
                f"У вас - Высокий уровень стресса \nколичество баллов - {total}"
            )

        return result_str

    def calculate_san(self, answers):
        exc = ["3", "4", "9", "10", "13", "15", "16", "21", "22", "27", "28"]
        activity = ["3", "4", "9", "10", "15", "16", "21", "22", "27", "28"]
        health = ["1", "2", "7", "8", "13", "14", "19", "20", "25", "26"]
        mood = ["5", "6", "11", "12", "17", "18", "23", "24", "29", "30"]
        result_str = ""

        health_total = 0
        activity_total = 0
        mood_total = 0

        for key in answers:
            if key in exc:
                answers[key] = abs(answers[key] - 8)

            if key in health:
                health_total = health_total + answers[key]
            elif key in activity:
                activity_total = activity_total + answers[key]
            elif key in mood:
                mood_total = mood_total + answers[key]

        total = ((mood_total / 10) + (activity_total / 10) +
                 (health_total / 10)) / 3
        total = round(total, 2)

        if total >= 4:
            result_str = f"Ваше эмоциональное состояние - Благоприятное :)! \n количество баллов - {total}"
        else:
            result_str = f"Ваше эмоциональное состояние - Неблагоприятное :< ! \n количество баллов - {total}"

        return result_str

    def calculate_psm(self, answers):
        exc = ["14"]
        total = 0
        result_str = ""

        for key in answers:
            if key in exc:
                answers[key] = abs(answers[key] - 9)
            total = answers[key] + total

        if total >= 155:
            result_str = (
                f"У вас - Высокий уровень стресса \nколичество баллов - {total}"
            )
        elif 100 <= total < 155:
            result_str = (
                f"У вас - Средний уровень стресса \nколичество баллов - {total}"
            )

        else:
            result_str = f"У вас - Низкий уровень стресса \nколичество баллов - {total}"

        return result_str

    def calculate_express(self, answers):
        total = 0
        result_str = ""

        for key in answers:
            total = answers[key] + total

        if total <= 4:
            result_str = (
                f"У вас - низкий уровень стресса \nколичество баллов - {total}"
            )
        elif 4 < total <= 7:
            result_str = (
                f"У вас - средний уровень стресса \nколичество баллов - {total}"
            )
        else:
            result_str = (
                f"У вас - высокий уровень стресса \nколичество баллов - {total}"
            )

        return result_str


class ResultCard(MDCard):
    pass
