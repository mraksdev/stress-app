from kivy.uix.screenmanager import Screen
from kivymd.uix.card import MDCard
from kivy.properties import StringProperty
from assets.data.results_templates import PSM_RESULTS
import json


class PSMScreenView(Screen):
    pass


class PSMCard(MDCard):
    question = StringProperty()
    group = StringProperty()
    answ = None

    def set_answer(self, value):
        PSM_RESULTS[self.answ] = value
        json.dump(PSM_RESULTS, open("assets/data/results.json", "w"))
        type = {"type": 3}
        json.dump(type, open("assets/data/type.json", "w"))
