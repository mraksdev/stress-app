from kivymd.app import MDApp
from kivy.lang import Builder
from kivymd.uix.screenmanager import MDScreenManager
from View.screens import screens


class stress_app(MDApp):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.theme_cls.primary_palette = "Amber"
        self.theme_cls.primary_hue = "A700"
        self.root = MDScreenManager()
        self.load_screen("main screen")
        self.set_current_screen("main screen")

    def set_current_screen(self, name: str):
        if not self.root.has_screen(name):
            self.load_screen(name)
        if name == "result screen":
            rs = self.root.get_screen("result screen")
            rs.show_results()
        self.root.current = name

    def load_screen(self, name: str):
        Builder.load_file(screens[name]["kv"])
        self.root.add_widget(screens[name]["view"]())


if __name__ == "__main__":
    stress_app().run()
